#include "drawing.h"
#include <QtGui>
#include <QMessageBox>

using namespace std;

void UserDrawing::setColor(QColor c)
{
	m_color = c;
}

UserDrawing::~UserDrawing()
{


	for (auto it = userStrokes.begin(); it != userStrokes.end(); ++it) {
		delete *it;
	}
	userStrokes.clear();

	for (auto it = strokeSegments.begin(); it != strokeSegments.end(); ++it) {
		delete *it;
	}
	strokeSegments.clear();
}


void UserDrawingVisualisation::drawStrokeSegment(const QPointF &p_start, const QPointF &p_end, double pressure, QPixmap &pixmap)
{
	QPainter painter(&pixmap);

	painter.setRenderHint(QPainter::Antialiasing);
	m_pen.setWidthF(min(1.0 * pressure, 1.5) + 1.0);
	int alpha_val = int(min(1.8*pressure, 1.0) * 255);

	m_color.setAlpha(alpha_val);

	m_pen.setColor(m_color);
	painter.setPen(m_pen);
	painter.drawLine(p_start, p_end);
	painter.end();
}

void UserDrawingVisualisation::drawStrokeSegment(const QPointF &p_start, const QPointF &p_end, double pressure, QPixmap &pixmap, QColor &color)
{
	QPainter painter(&pixmap);
	painter.setRenderHint(QPainter::Antialiasing);

	double alpha = min(1.0, pressure);
	color.setAlphaF(alpha);	
	m_pen.setWidthF(pressure* pen_width);
	m_pen.setColor(color);
	painter.setPen(m_pen);

	painter.drawLine(p_start, p_end);
	painter.end();
}


void UserDrawingVisualisation::drawStrokeSegmentWidthNoAlpha(const QPointF &p_start, const QPointF &p_end, double pressure, QPixmap &pixmap, QColor &color)
{
	//Draw with proper width but without alpha information.

	QPainter painter(&pixmap);
	painter.setRenderHint(QPainter::Antialiasing);

	double alpha = 1.0;

	color.setAlphaF(alpha);

	m_pen.setWidthF(pressure* pen_width);
	m_pen.setColor(color);
	painter.setPen(m_pen);

	painter.drawLine(p_start, p_end);
	painter.end();
}

bool UserDrawingLabeling::displayLineType(QPixmap &label_image, int line_type, bool use_pressure)
{
	int seg_ind;
	int stroke_ind;
	int p1_ind;
	int p2_ind;

	double pressure = 1.0;

	int num_segments = 0;

	for (int j = 0; j < strokeSegments.size(); j++)
	{
		if (strokeSegments[j]->line_type_ != line_type)
			continue;

		num_segments++;
		//seg_ind = line_type_segments_nums[line_type][j];
		seg_ind = j;

		stroke_ind = strokeSegments[seg_ind]->userStrokeIndex;
		p1_ind = strokeSegments[seg_ind]->pointBeginIndex;
		p2_ind = strokeSegments[seg_ind]->pointEndIndex;

		if (use_pressure)
			//pressure = (userStrokes[stroke_ind]->points[p1_ind]->pressure + userStrokes[stroke_ind]->points[p2_ind]->pressure) / 2.0;
			pressure = (userStrokes[stroke_ind]->points[p2_ind]->pressure);

		drawStrokeSegment(QPointF(userStrokes[stroke_ind]->points[p1_ind]->x, userStrokes[stroke_ind]->points[p1_ind]->y),
			QPointF(userStrokes[stroke_ind]->points[p2_ind]->x, userStrokes[stroke_ind]->points[p2_ind]->y),
			pressure,
			label_image,
			labels_colors_[line_type]);
	}
	
	
	if (num_segments)
		return true;
	else
		return false;
};

bool UserDrawingLabeling::displayLineType(QPixmap &label_image, int line_type, QColor color, bool use_pressure) 
{

	int seg_ind;
	int stroke_ind;
	int p1_ind;
	int p2_ind;

	double pressure = 1.0;

	int num_segments = 0;

	for (int j = 0; j < strokeSegments.size(); j++)
	{
		if (strokeSegments[j]->line_type_ != line_type)
			continue;

		num_segments++;
	
		seg_ind = j;

		stroke_ind = strokeSegments[seg_ind]->userStrokeIndex;
		p1_ind = strokeSegments[seg_ind]->pointBeginIndex;
		p2_ind = strokeSegments[seg_ind]->pointEndIndex;

		if (use_pressure)
			//pressure = (userStrokes[stroke_ind]->points[p1_ind]->pressure + userStrokes[stroke_ind]->points[p2_ind]->pressure) / 2.0;
			pressure = (userStrokes[stroke_ind]->points[p2_ind]->pressure);

		drawStrokeSegment(QPointF(userStrokes[stroke_ind]->points[p1_ind]->x, userStrokes[stroke_ind]->points[p1_ind]->y),
			QPointF(userStrokes[stroke_ind]->points[p2_ind]->x, userStrokes[stroke_ind]->points[p2_ind]->y),
			pressure,
			label_image,
			color);
	}
	

	if (num_segments)
		return true;
	else
		return false;
}

bool  UserDrawingLabeling::displayLineTypes(QPixmap &label_image, bool use_pressure)
{

	int seg_ind;
	int stroke_ind;
	int p1_ind;
	int p2_ind;

	double pressure = 1.0;

	QColor color;

	for (int j = 0; j < strokeSegments.size(); j++)
	{
		color = labels_colors_[strokeSegments[j]->line_type_];

		seg_ind = j;

		stroke_ind	= strokeSegments[seg_ind]->userStrokeIndex;

		p1_ind	= strokeSegments[seg_ind]->pointBeginIndex;
		p2_ind	= strokeSegments[seg_ind]->pointEndIndex;

		if (use_pressure)
			//pressure = (userStrokes[stroke_ind]->points[p1_ind]->pressure + userStrokes[stroke_ind]->points[p2_ind]->pressure) / 2.0;
			pressure = (userStrokes[stroke_ind]->points[p2_ind]->pressure);

		drawStrokeSegmentWidthNoAlpha(QPointF(userStrokes[stroke_ind]->points[p1_ind]->x, userStrokes[stroke_ind]->points[p1_ind]->y),
			QPointF(userStrokes[stroke_ind]->points[p2_ind]->x, userStrokes[stroke_ind]->points[p2_ind]->y),
			pressure,
			label_image,
			color);
	}
	
	
	return true;
}

void UserDrawingVisualisation::drawStrokeSegment(int seg_ind, QPixmap &pixmap, QColor &color)
{
	
	int stroke_ind = strokeSegments[seg_ind]->userStrokeIndex;
	int p1_ind = strokeSegments[seg_ind]->pointBeginIndex;
	int p2_ind = strokeSegments[seg_ind]->pointEndIndex;
	double pressure = (userStrokes[stroke_ind]->points[p2_ind]->pressure);
	QPointF p_start(userStrokes[stroke_ind]->points[p1_ind]->x, userStrokes[stroke_ind]->points[p1_ind]->y);
	QPointF p_end(userStrokes[stroke_ind]->points[p2_ind]->x, userStrokes[stroke_ind]->points[p2_ind]->y);
	
	QPainter painter(&pixmap);
	painter.setRenderHint(QPainter::Antialiasing);

	//m_pen.setWidthF(min(0.65 * pressure, 0.65) + 0.45);
	//m_pen.setWidthF(pressure*2.0); //pen_width =2.0
	double alpha = min(1.0, pressure);
	color.setAlphaF(alpha);
	m_pen.setWidthF(pressure* pen_width);
	m_pen.setColor(color);
	painter.setPen(m_pen);

	painter.drawLine(p_start, p_end);
	painter.end();
}

void UserDrawingVisualisation::drawStrokeSegmentWidthNoAlpha(int seg_ind, QPixmap &pixmap, QColor &color)
{
	
	int stroke_ind  = strokeSegments[seg_ind]->userStrokeIndex;
	int p1_ind = strokeSegments[seg_ind]->pointBeginIndex;
	int p2_ind = strokeSegments[seg_ind]->pointEndIndex;
	double pressure = (userStrokes[stroke_ind]->points[p2_ind]->pressure);
	QPointF p_start(userStrokes[stroke_ind]->points[p1_ind]->x, userStrokes[stroke_ind]->points[p1_ind]->y);
	QPointF p_end(userStrokes[stroke_ind]->points[p2_ind]->x, userStrokes[stroke_ind]->points[p2_ind]->y);
	
	QPainter painter(&pixmap);
	painter.setRenderHint(QPainter::Antialiasing);

	double alpha = 1.0;
	color.setAlphaF(alpha);

	m_pen.setWidthF(pressure* pen_width);
	m_pen.setColor(color);
	painter.setPen(m_pen);

	painter.drawLine(p_start, p_end);
	painter.end();
}



void UserDrawingVisualisation::displaySketchInterval(QPixmap &sketch_pixmap, int first_stroke, int last_stroke, int base_label, double pressure_scale, bool use_pressure, bool use_label_color)
{
	//This funtion is displaying line types in the sketch interval, including last_stroke

	QPainter painter(&sketch_pixmap);
	painter.setRenderHint(QPainter::Antialiasing);

	QPointF lastPoint;
	QPointF newPoint;



	double p = 1.0;

	int ind_last_point, ind_first_point;
	int first_segment;
	int last_segment;
	m_color = QColor(0, 0, 0, 255);
	//seg_num = userStrokes[first_stroke]->first_segment_ind;
	

	for (int i = first_stroke; i <= last_stroke; i++)
	{
		
		if (userStrokes[i]->points.size() < 2)
		{
			continue;
		}

		if (userStrokes[i]->is_removed_)
			continue;


		first_segment = userStrokes[i]->first_segment_ind;
		last_segment = userStrokes[i]->last_segment_ind;


		for (int j = first_segment; j <= last_segment; j++)
		{
			ind_first_point = strokeSegments[j]->pointBeginIndex;
			ind_last_point = strokeSegments[j]->pointEndIndex;

			if (use_pressure)
				p = userStrokes[i]->points[ind_last_point]->pressure;
			else
				p = 1.0;

			if (strokeSegments[j]->line_type_ != base_label)
				p *= pressure_scale;

			if (use_label_color)
				m_color = labels_colors_[strokeSegments[j]->line_type_];

			m_pen.setWidthF(p * pen_width);
			m_color.setAlphaF(p);
			m_pen.setColor(m_color);

			lastPoint = QPointF(userStrokes[i]->points[ind_first_point]->x, userStrokes[i]->points[ind_first_point]->y);
			newPoint = QPointF(userStrokes[i]->points[ind_last_point]->x, userStrokes[i]->points[ind_last_point]->y);
			painter.setPen(m_pen);
			painter.drawLine(lastPoint, newPoint);
		}
	}
	painter.end();
}


void UserDrawingVisualisation::displayDrawing(QPixmap &sketch_pixmap, double pressure_scale)
{
	QPainter painter(&sketch_pixmap);
	painter.setRenderHint(QPainter::Antialiasing);
	m_color = QColor(0, 0, 0, 255);
	QPointF lastPoint;
	QPointF newPoint;

	double p;

	for (int i = 0; i < userStrokes.size(); i++)
	{
		if (userStrokes[i]->points.size() < 2)
		{
			continue;
		}

		for (int j = 0; j < userStrokes[i]->points.size() - 1; j++)
		{
			if (userStrokes[i]->is_removed_)
				continue;

			p = userStrokes[i]->points[j + 1]->pressure*pressure_scale;

			m_pen.setWidthF(p * pen_width);

			m_color.setAlphaF(p);
			m_pen.setColor(m_color);

			lastPoint = QPointF(userStrokes[i]->points[j]->x, userStrokes[i]->points[j]->y);
			newPoint = QPointF(userStrokes[i]->points[j + 1]->x, userStrokes[i]->points[j + 1]->y);
			painter.setPen(m_pen);
			painter.drawLine(lastPoint, newPoint);
		}
	}
	painter.end();
};


void parseSketchJSON(const QString filename, UserDrawingLabeling* drawing)
{
	QString fileContent;
	QFile file;
	file.setFileName(filename);
	file.open(QIODevice::ReadOnly | QIODevice::Text);
	fileContent = file.readAll();
	file.close();

	QJsonDocument doc = QJsonDocument::fromJson(fileContent.toUtf8()); //[TODO::] hande error
	QJsonObject root = doc.object();

	//Read canvas data:
	QJsonObject canvasObject = root.value(QString("canvas")).toObject();

	drawing->canvas_height = canvasObject["height"].toInt();
	drawing->canvas_width = canvasObject["width"].toInt();
	drawing->thickness = canvasObject["pen_width"].toDouble();


	//Read strokes data
	QJsonArray strokesArray = root.value(QString("strokes")).toArray();

	if (strokesArray.size() == 0)
	{
		return;
	}

	Stroke* stroke;
	int index = 0;
	int index_non_removed = 0;
	int num_segments;
	for (int stroke_index = 0; stroke_index < static_cast<int>(strokesArray.size()); ++stroke_index)
	{
		
		stroke = new Stroke();
		drawing->userStrokes.push_back(stroke);
		drawing->userStrokes.back()->index = index++;

		QJsonObject strokeObject = strokesArray[stroke_index].toObject();

		QJsonArray pointsArray = strokeObject["points"].toArray();
		stroke->is_removed_ = strokeObject["is_removed"].toBool();
		stroke->ind_non_removed = index_non_removed;
		
		if (!stroke->is_removed_)
			index_non_removed++;
	
		num_segments = 0;
		for (int j = 0; j < static_cast<int>(pointsArray.size()); ++j)
		{
			QJsonObject pointObject = pointsArray[j].toObject();
			
			PointDraw* point = new PointDraw(	pointObject.value(QString("x")).toDouble(),
												pointObject.value(QString("y")).toDouble(),
												pointObject.value(QString("p")).toDouble(),
												pointObject.value(QString("t")).toDouble());

			drawing->userStrokes.back()->points.push_back(point);		

			if (j>0)
			{
				StrokeSegment* segment = new StrokeSegment(j-1, j, stroke_index);
				drawing->strokeSegments.push_back(segment);
				num_segments = num_segments + 1;				
			}
		}
		drawing->userStrokes[stroke_index]->first_segment_ind = drawing->strokeSegments.size() - num_segments;
		drawing->userStrokes[stroke_index]->last_segment_ind = drawing->strokeSegments.size() - 1;
	}

}


void  UserDrawingLabeling::loadStrokesLabels(QString filename)
{
	//Read labels from a json file:
	QFile jsonFile;
	jsonFile.setFileName(filename);
	jsonFile.open(QIODevice::ReadOnly | QIODevice::Text);
	QString fileContent = jsonFile.readAll();
	jsonFile.close();
	QJsonDocument doc = QJsonDocument::fromJson(fileContent.toUtf8());
	QJsonObject root = doc.object();
	
	
	QJsonArray strokesLabelsArray = root.value(QString("strokes_line_types")).toArray();
	cout << "Number of labels in the drawing " << static_cast<int>(strokesLabelsArray.size()) << endl;
	//Assign labels:
	int line_type;
	int ind_non_removed_strokes = 0;
	for (int i = 0; i < static_cast<int>(userStrokes.size()); ++i)
	{		
		if (userStrokes[i]->is_removed_)
			continue;

		

		// Assign stroke label:
		line_type = strokesLabelsArray[ind_non_removed_strokes].toInt();
		userStrokes[i]->line_type_ = line_type;

		// Assign labels to segments:
		for (int si = userStrokes[i]->first_segment_ind; si <= userStrokes[i]->last_segment_ind; si++)
		{
			strokeSegments[si]->line_type_ = line_type;
		}
		ind_non_removed_strokes++;
	}

}


std::string get_file_contents(const char *filename)
{
	std::ifstream in(filename, std::ios::in | std::ios::binary);
	if (in)
	{
		std::string contents;
		in.seekg(0, std::ios::end);
		contents.resize(in.tellg());
		in.seekg(0, std::ios::beg);
		in.read(&contents[0], contents.size());
		in.close();
		return(contents);
	}
	throw(errno);
}

void labelToColorRGB(double &r, double &g, double &b, double label, double num_labels)
{
	r = 1.0;
	g = 1.0; 
	b = 1.0;

	double v = label / num_labels;

	if (v < 0.2) {
		r = 0;
		g = 5 * v;
	}
	else if (v < 0.4) {
		r = 0;
		b = 1 + 5 * (0.2 - v);
	}
	else if (v < 0.6) {
		r = 5 * (v - 0.4);
		b = 0;
	}
	else if (v < 0.8 ) {
		g = 1 - 5 * (v - 0.6);
		b = 0;
	}
	else {
		g = 0;
		b = 5 * (v - 0.8);
	}
}

QColor labelToColorQt(double label, double num_labels)
{
	label = (label == -1) ? 0 : label;

	double r = 1.0, g = 1.0, b = 1.0;

	labelToColorRGB(r, g, b, label, num_labels);

	//cout << "(r,g,b) = (" << r << "," << g << "," << b << ")" << endl;
	return QColor(r * 255, g * 255, b * 255);
}


void keepOnlyNonRemovedStrokes(UserDrawingLabeling* sketch)
{
	for (int s = 0; s < sketch->userStrokes.size(); )
	{
		cout << "num_strokes = " << sketch->userStrokes.size() << endl;
		cout << "s = " << s << endl;
		if (sketch->userStrokes[s]->is_removed_)
			sketch->userStrokes.erase(sketch->userStrokes.begin() + s);
		else
			s++;
	}
}