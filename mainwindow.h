/* The MainWindow class inherits QMainWindow, creates the menus, and connects their slots and signals.*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QColorDialog;

class TabletCanvas;
#include "globals.h"
#include "drawing.h"



class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(TabletCanvas *canvas);

private:
	void about();
	void save();

	void startLinesLabeling();
	void loadLinesLabeling();
	void loadJSON(); //loads from a single json file, generated with qtjson

private:
	
	TabletCanvas *m_canvas;
	UserDrawingLabeling* drawing;

	string drawing_folder_;
	string file_name_;

	
	bool loadImage(QPixmap &pixmap, const QString &file);
	bool loadSingleJSON(const QString &file); 

	void createMenus();
};

#endif