#include <QtWidgets>
#include "mainwindow.h"
#include "tabletcanvas.h"
#include "linesLabeling.h"

using namespace std;

MainWindow::MainWindow(TabletCanvas *canvas)
: m_canvas(canvas)
{
	createMenus();
	setWindowTitle(tr("Input Drawing"));
	setCentralWidget(m_canvas);
	drawing = new UserDrawingLabeling(); 	
	m_canvas->drawing = drawing;
}


void MainWindow::save()
{
	//Saves a json file and a png image to a selected file.

	QString path = QDir::currentPath() + "/untitled.png";
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Picture"),
		path);

	if (!m_canvas->saveImage(fileName))
		QMessageBox::information(this, "Error Saving Picture",
		"Could not save the image");	
}

bool MainWindow::loadSingleJSON(const QString &file)
{
	//----------------------------------------
	//Read json file
	parseSketchJSON(file, drawing); // the version with removed strokes
	filename_global = file.toStdString();
	QPixmap pixmapDrawing = QPixmap(drawing->canvas_width, drawing->canvas_height);;
	drawing->displayDrawing(pixmapDrawing);
	//----------------------------------------
	//Display corresponding image
	
	m_canvas->displayPixmap(pixmapDrawing);
	//----------------------------------------
	return true;
}


void MainWindow::loadJSON()
{
	QString fileFullName = QFileDialog::getOpenFileName(this, tr("Open JSON file"),
													tr(".\\"));

	QFileInfo fi(fileFullName);
	file_name_ = fi.fileName().toStdString();
	drawing_folder_ = fi.path().toStdString();

	if (!loadSingleJSON(fileFullName))
		QMessageBox::information(this, "Error loading json file", "Could not load json file");

	this->resize(m_canvas->width(), m_canvas->height());
}



bool MainWindow::loadImage(QPixmap &pixmap, const QString &file)
{
	bool success = pixmap.load(file);

	if (!success)
		return false;

	//Original image size:
	drawing->canvas_height = pixmap.height();
	drawing->canvas_width = pixmap.width();
	

	//Resize the image to fit screen:
	int width = pixmap.width();
	int height = pixmap.height();

	if (height > 1000)
	{
		width *= 1000.0 / double(height);
		height = 1000;
	}

	pixmap = pixmap.scaled(width, height, Qt::KeepAspectRatio);

	return true;
}


void MainWindow::about()
{
	QMessageBox::about(this, tr("About sketch annotation tool"),
		tr("todo :: add help"));
}


void MainWindow::startLinesLabeling()
{
	QString folder = QFileDialog::getExistingDirectory(this, tr("Select folder to save a labeling"),
																tr("//"));

	LineLabeling *newPage = new LineLabeling(drawing, folder.toStdString());
	newPage->show();
}


void MainWindow::loadLinesLabeling()
{
	
	QString fileName = QFileDialog::getOpenFileName(this,	tr("Load lines labelling"),
															tr("//") );

	drawing->loadStrokesLabels(fileName);

	//Select the folder to save labeling to:
	QString folder = QFileDialog::getExistingDirectory(this, tr("Select folder for semantic annotation"),
															 tr("//"));

	//Open the window:
	LineLabeling *newPage = new LineLabeling(drawing, folder.toStdString());
	newPage->show();
}


void MainWindow::createMenus()
{
	QMenu *fileMenu = menuBar()->addMenu(tr("&File"));
	fileMenu->addAction(tr("&Open json..."), this, &MainWindow::loadJSON, QKeySequence::Open);	
	fileMenu->addAction(tr("&Save as pnd..."), this, &MainWindow::save, QKeySequence::SaveAs);	
	fileMenu->addAction(tr("E&xit"), this, &MainWindow::close, QKeySequence::Quit);	


	QMenu *annotateMenu = menuBar()->addMenu(tr("&Labeling"));
	annotateMenu->addAction(tr("&Start strokes labeling"), this, &MainWindow::startLinesLabeling, QKeySequence(tr("")));
	annotateMenu->addAction(tr("&Load strokes labeling"), this, &MainWindow::loadLinesLabeling, QKeySequence(tr("")));		
	
	QMenu *helpMenu = menuBar()->addMenu("&Help");
	helpMenu->addAction(tr("A&bout"), this, &MainWindow::about);
	helpMenu->addAction(tr("About &Qt"), qApp, &QApplication::aboutQt);
}