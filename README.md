This is the code for the labeling tool used in OpenSketh paper. 
To compile the code please use cmake on CMakeLists.txt.

## This code is a part of OpenSketch publication
Project page:
https://ns.inria.fr/d3/OpenSketch/

Additional data:
https://repo-sam.inria.fr/d3/OpenSketch/


## Contact

If you have any questions please contact Yulia Gryaditskaya yulia.gryaditskaya@gmail.com