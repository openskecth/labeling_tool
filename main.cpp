/*
Creates a MainWinow and display it as a top level window.
*/

#include <QtWidgets>


#include "tabletapplication.h"
#include "mainwindow.h"
#include "tabletcanvas.h"


int main(int argc, char* argv[])
{
	TabletApplication app(argc, argv);

	int height = 800,
		width = 600;

	TabletCanvas *canvas = new TabletCanvas(height, width);
	app.setCanvas(canvas);
	MainWindow mainWindow(canvas);
	
	mainWindow.resize(canvas->width(), canvas->height());
	mainWindow.show();
	return app.exec();	
}
