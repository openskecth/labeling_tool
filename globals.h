#ifndef GLOBALS_H
#define GLOBALS_H

#include <iostream>
#include <fstream>
#include <QColor>
#include <map>

using namespace std;

extern string filename_global;

const int num_types_of_lines = 26;

enum LineType {
	//discriptive
	l_occluding_contour,
	l_ridges_visible,
	l_vallleys_visible,
	l_ridges_occluded,
	l_valleys_occluded,
	l_cross_sections,
	//context
	l_context_axis_and_grids,
	l_context_scaffolds,
	l_context_scaffolds_lines_to_vp,
	l_context_scaffold_square,
	l_context_scaffold_circle,
	//Surfacing
	l_surfacing_cross_section,
	l_surfacing_cross_section_multiple_rounding,
	l_surfacing_mirroring,
	l_surfacing_temporary_plane,
	l_surfacing_projection_lines,
	//Propotions
	l_propotions_div_rectangle2,
	l_propotions_div_rectangle3,
	l_propotions_div_ellipse,
	l_propotions_mult_rectangle,
	l_hinging_and_rotating_elements,
	/*l_propotions_mirror_in_plane,
	l_propotions_mirror_2Dcurve,
	l_propotions_mirror_tilted_plane,
	l_propotions_mirror_2D_tilted_curves,
	l_propotions_mirror_3D_curves,*/
	l_ticks,
	//Additional
	l_hatching,
	l_text,
	//additional label
	l_outline,
	l_shadow_construction,
	l_background, 
	l_removed
};

//First and last elemnt number in the enum
const int discriptive[2] = { 0, 5 };
const int context[2] = { 6, 10 };
const int surfacing[2] = { 11, 14 };
const int propotions[2] = { 15, 23 };

extern QColor labels_colors_[26];
extern string  line_type_name[26];

struct mapLinesTypes
{
	map<string, int> mlt;

public:
	mapLinesTypes()
	{
		for (int i = 0; i <= l_shadow_construction; i++)
		{
			mlt[line_type_name[i]] = i;
		}

		//Group cross-sections
		mlt["surfacing_cross_section"] = l_cross_sections;

		mlt["mult_rectangle"] = l_propotions_div_rectangle2;
	};

};



#endif