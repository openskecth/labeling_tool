#include <QtWidgets>
#include <math.h>

#include "tabletcanvas.h"

#include <ctime>
#include <json.h>

using namespace std;

TabletCanvas::TabletCanvas(int height, int width)
: QWidget(Q_NULLPTR)
, m_alphaChannelValuator(TangentialPressureValuator)
, m_colorSaturationValuator(NoValuator)
, m_lineWidthValuator(PressureValuator)
, m_color(QColor(130, 130, 130))
, m_brush(m_color)
, pen_width(2.0)
, m_pen(m_brush, pen_width, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin)
, m_deviceDown(false)
, stroke_index(0)
{
	drawing = new UserDrawingLabeling();
	m_height = height;
	m_width = width;

	//Resize the widget window:
	resize(m_width, m_height);
	initPixmap();
	setAutoFillBackground(false);

	//time(&startTime);
	startTime = clock();
	currTime = 0;
	
}

TabletCanvas::~TabletCanvas()
{
	
	delete drawing;
	
}

void TabletCanvas::clear()
{
	resize(m_width, m_height);
	//initPixmap();
	//setAutoFillBackground(true);

	startTime = clock();
	currTime = clock();

	delete drawing;
}

//void TabletCanvas::serialize(QString filename)
//{
//	std::ofstream file_id(filename.toStdString().c_str());
//
//	Json::Value root;
//	root["canvas"]["pen_width"] = pen_width;
//	root["canvas"]["width"] = m_width;
//	root["canvas"]["height"] = m_height;
//	//file_id << canvas << endl;
//
//	
//	//Json::Value stroke;
//
//	for (int i = 0; i < drawing->userStrokes.size(); i++)
//	{
//		//cout << "Stroke[" << i+1 << "/" << userStrokes.size() << "] contains ";
//		//cout << userStrokes[i]->points.size() << "points" << endl;
//		for (int j = 0; j < drawing->userStrokes[i]->points.size(); j++)
//		{			
//			root["strokes"][i]["points"][j]["x"] = drawing->userStrokes[i]->points[j]->x;
//			root["strokes"][i]["points"][j]["y"] = drawing->userStrokes[i]->points[j]->y;
//			root["strokes"][i]["points"][j]["pressure"] = drawing->userStrokes[i]->points[j]->pressure;
//			root["strokes"][i]["points"][j]["interval"] = drawing->userStrokes[i]->points[j]->interval;
//		}		
//	}	
//	file_id << root << endl;
//	file_id.close();	
//}
//



bool TabletCanvas::saveImage(const QString &file)
{
	bool success = m_pixmap.save(file);

	//if (success)
	//{
	//	QString dataFile(file);
	//	dataFile.replace(QString(".png"), QString(".json"));
	//	serialize(dataFile);
	//	return true;
	//}
	
	return true;
}

void TabletCanvas::initPixmap()
{
	QPixmap newPixmap = QPixmap(m_width, m_height);
	newPixmap.fill(Qt::transparent);
	QPainter painter(&newPixmap);
	if (!m_pixmap.isNull())
		painter.drawPixmap(0, 0, m_pixmap);
	painter.end();
	m_pixmap = newPixmap;
	update();
}

void TabletCanvas::updatePixmap()
{
	QPixmap newPixmap = QPixmap(m_width, m_height);
	newPixmap.fill(Qt::transparent);
	QPainter painter(&newPixmap);
	painter.end();
	m_pixmap = newPixmap;
	update();
}

void TabletCanvas::displayPixmap(QPixmap &pixmap)
{		
	m_pixmap = pixmap;

	this->m_width  = m_pixmap.width();
	this->m_height = m_pixmap.height();
	//cout << m_pixmap.width() << " " << m_pixmap.height() << endl;
	//resize(m_pixmap.width(), m_pixmap.height());
	
	//QPainter painter(&pixmap);
	//painter.drawPixmap(0, 0, pixmap);
	//painter.end();


	update();
}

void TabletCanvas::tabletEvent(QTabletEvent *event)
{

	switch (event->type()) {
	case QEvent::TabletPress:
		if (!m_deviceDown) {
			m_deviceDown = true;
			lastPoint.pos = event->posF();
			lastPoint.rotation = event->rotation();

			//Create new stroke.
			Stroke *stroke = new Stroke();
			drawing->userStrokes.push_back(stroke);
			drawing->userStrokes.back()->index = stroke_index++;
		}
		break;
	case QEvent::TabletMove:
		if (event->device() == QTabletEvent::RotationStylus)
			updateCursor(event);
		if (m_deviceDown) {
			updateBrush(event);
			QPainter painter(&m_pixmap);
			paintPixmap(painter, event);
			
			lastPoint.pos = event->posF();
			lastPoint.rotation = event->rotation();

			currTime = clock();
			int timeElapsed = (currTime - startTime) / double(CLOCKS_PER_SEC) * 1000 ; // in miliseconds

			//Create new point.
			PointDraw* point = new PointDraw(lastPoint.pos.x(),
				lastPoint.pos.y(),
				event->pressure(),
				timeElapsed,
				drawing->cur_line_type_);
			
			drawing->userStrokes.back()->points.push_back(point);
		}
		break;
	case QEvent::TabletRelease:
		if (m_deviceDown && event->buttons() == Qt::NoButton)
			m_deviceDown = false;
		break;
	default:
		break;
	}
	event->accept();
	update();
}

void TabletCanvas::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.drawPixmap(0, 0, m_pixmap);
}

/*
In this function we draw on the pixmap based on the movement of the tool.If the tool used on the 
tablet is a stylus, we want to draw a line from the last - known position to the current position.
We also assume that this is a reasonable handling of any unknown device, but update the status bar 
with a warning.
*/
void TabletCanvas::paintPixmap(QPainter &painter, QTabletEvent *event)
{
	painter.setRenderHint(QPainter::Antialiasing);

	switch (event->device()) {
	case QTabletEvent::Airbrush:
	{
			painter.setPen(Qt::NoPen);
			QRadialGradient grad(lastPoint.pos, m_pen.widthF() * 10.0);
			QColor color = m_brush.color();
			color.setAlphaF(color.alphaF() * 0.25);
			grad.setColorAt(0, m_brush.color());
			grad.setColorAt(0.5, Qt::transparent);
			painter.setBrush(grad);
			qreal radius = grad.radius();
			painter.drawEllipse(event->posF(), radius, radius);
	}
		break;
	case QTabletEvent::RotationStylus:
	{
			m_brush.setStyle(Qt::SolidPattern);
			painter.setPen(Qt::NoPen);
			painter.setBrush(m_brush);
			QPolygonF poly;
			qreal halfWidth = m_pen.widthF();
			QPointF brushAdjust(qSin(qDegreesToRadians(lastPoint.rotation)) * halfWidth,
				qCos(qDegreesToRadians(lastPoint.rotation)) * halfWidth);
			poly << lastPoint.pos + brushAdjust;
			poly << lastPoint.pos - brushAdjust;
			brushAdjust = QPointF(qSin(qDegreesToRadians(event->rotation())) * halfWidth,
				qCos(qDegreesToRadians(event->rotation())) * halfWidth);
			poly << event->posF() - brushAdjust;
			poly << event->posF() + brushAdjust;
			painter.drawConvexPolygon(poly);
	}
		break;
	case QTabletEvent::Puck:
	case QTabletEvent::FourDMouse:
	{
									 const QString error(tr("This input device is not supported by the example."));
#ifndef QT_NO_STATUSTIP
									 QStatusTipEvent status(error);
									 QApplication::sendEvent(this, &status);
#else
									 qWarning() << error;
#endif
	}
		break;
	default:
	{
			   const QString error(tr("Unknown tablet device - treating as stylus"));
#ifndef QT_NO_STATUSTIP
			   QStatusTipEvent status(error);
			   QApplication::sendEvent(this, &status);
#else
			   qWarning() << error;
#endif
	}
		// FALL-THROUGH
	case QTabletEvent::Stylus:
		painter.setPen(m_pen);
		painter.drawLine(lastPoint.pos, event->posF());
		break;
	}
}

void TabletCanvas::updateBrush(const QTabletEvent *event)
{
	int hue, saturation, value, alpha;
	m_color.getHsv(&hue, &saturation, &value, &alpha);

	int vValue = int(((event->yTilt() + 60.0) / 120.0) * 255);
	int hValue = int(((event->xTilt() + 60.0) / 120.0) * 255);

	switch (m_alphaChannelValuator) {
	case PressureValuator:
		m_color.setAlphaF(event->pressure());
		break;
	case TangentialPressureValuator:
		if (event->device() == QTabletEvent::Airbrush)
			m_color.setAlphaF(qMax(0.01, (event->tangentialPressure() + 1.0) / 2.0));
		else
			m_color.setAlpha(255);
		break;
	case TiltValuator:
		m_color.setAlpha(maximum(abs(vValue - 127), abs(hValue - 127)));
		break;
	case NoPressureValuator:
		m_color.setAlpha(255);
	default:
		m_color.setAlpha(255);
	}

	switch (m_colorSaturationValuator) {
	case VTiltValuator:
		m_color.setHsv(hue, vValue, value, alpha);
		break;
	case HTiltValuator:
		m_color.setHsv(hue, hValue, value, alpha);
		break;
	case PressureValuator:
		m_color.setHsv(hue, int(event->pressure() * 255.0), value, alpha);
		break;
	default:
		;
	}

	switch (m_lineWidthValuator) {
	case PressureValuator:
		//m_pen.setWidthF(event->pressure() * 10 + 1);
		m_pen.setWidthF(event->pressure() * pen_width);
		break;
	case TiltValuator:
		m_pen.setWidthF(maximum(abs(vValue - 127), abs(hValue - 127)) / 12);
		break;
	default:
		m_pen.setWidthF(pen_width);
	}

	if (event->pointerType() == QTabletEvent::Eraser) {
		m_brush.setColor(Qt::white);
		m_pen.setColor(Qt::white);
		m_pen.setWidthF(event->pressure() * 10 + 1);
	}
	else {
		m_brush.setColor(m_color);
		m_pen.setColor(m_color);
	}
}

void TabletCanvas::updateCursor(const QTabletEvent *event)
{
	QCursor cursor;
	if (event->type() != QEvent::TabletLeaveProximity) {
		if (event->pointerType() == QTabletEvent::Eraser) {
			cursor = QCursor(QPixmap(":/images/cursor-eraser.png"), 3, 28);
		}
		else {
			switch (event->device()) {
			case QTabletEvent::Stylus:
				cursor = QCursor(QPixmap(":/images/cursor-pencil.png"), 0, 0);
				break;
			case QTabletEvent::Airbrush:
				cursor = QCursor(QPixmap(":/images/cursor-airbrush.png"), 3, 4);
				break;
			case QTabletEvent::RotationStylus: {
												   QImage origImg(QLatin1String(":/images/cursor-felt-marker.png"));
												   QImage img(32, 32, QImage::Format_ARGB32);
												   QColor solid = m_color;
												   solid.setAlpha(255);
												   img.fill(solid);
												   QPainter painter(&img);
												   QTransform transform = painter.transform();
												   transform.translate(16, 16);
												   transform.rotate(-event->rotation());
												   painter.setTransform(transform);
												   painter.setCompositionMode(QPainter::CompositionMode_DestinationIn);
												   painter.drawImage(-24, -24, origImg);
												   painter.setCompositionMode(QPainter::CompositionMode_HardLight);
												   painter.drawImage(-24, -24, origImg);
												   painter.end();
												   cursor = QCursor(QPixmap::fromImage(img), 16, 16);
			} break;
			default:
				break;
			}
		}
	}
	setCursor(cursor);
}

void TabletCanvas::resizeEvent(QResizeEvent *)
{
	initPixmap();
}