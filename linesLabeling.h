#ifndef LinesLabeling_H
#define LinesLabeling_H

#include <QDialog>
#include <QtWidgets>
#include "tabletcanvas.h"
#include "json.h"
//#include "GCoptimization.h"
#include "drawing.h"
#include "string.h"

using namespace std;

class QGroupBox;
class QAction;
class QSlider;

class LineLabeling :
	public QDialog
{
	Q_OBJECT
private:
	void createRuddioBttnLabel(int label, const string s, QVBoxLayout *vbox, QSignalMapper* signalMapper);
	void createRuddioBttn(int label, const string s, QVBoxLayout *vbox, QSignalMapper* signalMapper);
private:
	int canvas_dim; // fore resized images
	void scaleDrawing(QPixmap &pixmap);

	string folder; //folder to save the data

	TabletCanvas *sketch_canvas;
	TabletCanvas *to_label_canvas;

	QSlider *transparency_slider;
	double transparency; 

	QSlider *time_scroll_slider_top;
	QSlider *time_scroll_slider_bottom;

	QSpinBox *stroke_last_box;
	QSpinBox *stroke_first_box;

	int num_strokes;

	int base_label;
	int assign_label;

	vector<int> num_seg_to_label;

	QGroupBox *createExclusiveLabelColorGroup();
	QGroupBox *createExclusiveLabelDisplayGroup();
	QGroupBox* createDisplayPannelGroup();

	QGroupBox *createExclusivePressureGroup();

	QGroupBox* createLabelPannelGroup();

	QGroupBox* createMenuGroup();

	bool display_pressure;
	
	QRadioButton* lines_types[num_types_of_lines];
	QRadioButton* lines_types_label[num_types_of_lines];

	QPushButton* assignBttn;

	QPushButton* saveImgsBttn;
	QPushButton* saveImgsGroupedBttn;
	QPushButton* saveImgsTimeBttn;

	QRadioButton *pressure_yes;
	QRadioButton *pressure_no;

	QGroupBox *gridGroupBox;
	QGridLayout *layout;
	QVBoxLayout *mainLayout;

	void set_label_color(QRadioButton *bttn, int label);

	void createLayout();
	void intialLabeling();

	void saveLabeling();
	
	void saveInitialDrawing();

	void selectSegmentsToLabel(int line_type);


protected slots:
	void saveLabelingImages();
	void saveLabelingImagesGrouped();
	void saveImagesSelectedInterval();

	void assignLabelToSelection();
	void display(int line_type);

	void setLabel(int label);

	void setDisplayPressure(int val) {
		display_pressure = (bool)val; 
		displayStrokeRange();
	};
	

	void changeTransparency(int alpha);
	void displayStrokeRange(int val=0);
	void displayStrokeRangeBox();
public:
	LineLabeling(UserDrawingLabeling *sketch, const string &folder);
	~LineLabeling();
};


#endif
