#ifndef DRAWING_H
#define DRAWING_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <QPen>

//json library
#include "json.h"
#include "globals.h"

using namespace std;




class PointDraw
{
public:
	double x, y;

	double pressure;

	int line_type_;
	double interval; //time indicator

	PointDraw(double argx = 0, double argy = 0, double argPressure = 0, double argInterval = 0, int line_type = -1)
		:x(argx), y(argy), pressure(argPressure), interval(argInterval), line_type_(line_type) {};
	~PointDraw() {};
};


class StrokeSegment
{
public:
	int pointBeginIndex;
	int pointEndIndex;
	int userStrokeIndex;	
	int line_type_;

	StrokeSegment(int pointBeginIndexArg = 0, int pointEndIndexArg = 0, int userStrokeIndexArg = 0, int line_type_arg = -1) :
		pointBeginIndex(pointBeginIndexArg),
		pointEndIndex(pointEndIndexArg),
		userStrokeIndex(userStrokeIndexArg),
		line_type_(line_type_arg)
	{};

	~StrokeSegment() { };
};

struct Stroke
{
public:
	std::vector< PointDraw* > points;
	
	int index;
	int first_segment_ind;
	int last_segment_ind;

	int line_type_;
	bool is_removed_;
	int ind_non_removed;

	
	Stroke(): 
		index(0), 
		first_segment_ind(0), 
		last_segment_ind(0),
		
		line_type_(l_removed),
		is_removed_(false),
		ind_non_removed(-1)
	{};

	~Stroke() {		
		for (auto it = points.begin(); it != points.end(); ++it) {
			delete *it;
		}
		points.clear();		
	};
};


struct Segment
{
	PointDraw p_begin;
	PointDraw p_end;

	Segment(StrokeSegment* segment_s, std::vector< Stroke* > userStrokes)
	{
		int stroke_ind = segment_s->userStrokeIndex;

		int ind_p1 = segment_s->pointBeginIndex;
		int ind_p2 = segment_s->pointEndIndex;

		p_begin = *(userStrokes[stroke_ind]->points[ind_p1]);
		p_end	= *(userStrokes[stroke_ind]->points[ind_p2]);
	}
};


class UserDrawing
{
protected: 
	QColor m_color;		
	double pen_width;
	QPen m_pen;

public:

	int canvas_width;
	int canvas_height;
	int thickness;	

	//Drawing representation:
	std::vector< Stroke* > userStrokes;
	std::vector< StrokeSegment* > strokeSegments;

	void setColor(QColor c);

	UserDrawing() :
		thickness(3)		
		, m_color(QColor(0, 0, 0, 255))
		, pen_width(1.5)
	{}

	~UserDrawing() ;
};


class UserDrawingVisualisation : public UserDrawing
{
	
public:

	void drawStrokeSegment(const QPointF &p_start, const QPointF &p_end, double pressure, QPixmap &pixmap);
	void drawStrokeSegment(const QPointF &p_start, const QPointF &p_end, double pressure, QPixmap &pixmap, QColor &color);
	void drawStrokeSegmentWidthNoAlpha(const QPointF &p_start, const QPointF &p_end, double pressure, QPixmap &pixmap, QColor &color);
	void drawStrokeSegmentWidthNoAlpha(int seg_ind, QPixmap &pixmap, QColor &color);
	
	void drawStrokeSegment(int seg_ind, QPixmap &pixmap, QColor &color);
	void displayDrawing(QPixmap &image, double pressure_scale = 1.0);

	void displaySketchInterval(QPixmap &sketch_pixmap, int first_stroke, int last_stroke, int base_label, double pressure_scale = 1.0, bool use_pressure = true, bool use_label_color = true); 
	
	UserDrawingVisualisation(): UserDrawing() {};

	~UserDrawingVisualisation(){};
};


class UserDrawingLabeling : public UserDrawingVisualisation
{
public:

	bool displayLineType(QPixmap &label_image, int line_type, bool use_pressure = false);
	bool displayLineType(QPixmap &label_image, int line_type, QColor color, bool use_pressure = false);
	bool displayLineTypes(QPixmap &label_image, bool use_pressure = false);

	int cur_line_type_; 
	void loadStrokesLabels(QString filename);

public:
	UserDrawingLabeling() : UserDrawingVisualisation(){}
	~UserDrawingLabeling() {};
};

QColor labelToColorQt(double label, double num_labels);

std::string get_file_contents(const char *filename);

void parseSketchJSON(const QString filename, UserDrawingLabeling* drawing);


#endif


//void keepOnlyNonRemovedStrokes(UserDrawingLabeling* skecth);