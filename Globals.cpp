#include "globals.h"
#include <map>


string filename_global = "file.json";

enum label;


QColor labels_colors_[26] =
{	
	QColor( 44, 108,   0),		//forest green	l_occluding_contour
	QColor(  0,  40, 140),		//dark blue		l_ridges_visible,
	QColor(220,  20,  60),		//crimson		l_vallleys_visible,
	QColor(  0, 149, 255),	    //light blue	l_ridges_occluded,
	QColor(255, 184,  29),		//yellow		l_valleys_occluded,
	QColor(170,   0, 255),		//violet		l_cross_sections		
	//Context
	QColor(127,138, 55),	//yellow-green		l_context_axis_and_grids,
	QColor(0,250,154),		//MediumSpringGreen	l_context_scaffolds,
	QColor(255, 109,  16),	    //orange		l_context_scaffolds_lines_to_vp,
	QColor(255,212,0),		//gold				l_context_scaffold_square,
	QColor(0,234,255),		//light sky blue	l_context_scaffold_circle,
	//Surfacing
	QColor(0, 128, 128),	//dark blue			l_surfacing_cross_section,
	QColor(132,215,187),	//aqua				l_surfacing_multiple_rounding,
	QColor(150,149,211),		//indigo			l_propotions_mirroring,
	QColor(221,160,221),	//plum				l_surfacing_temporary_plane,
	QColor(106,255,0),		//lime				l_surfacing_projection_lines,
	//Propotions
	QColor(255,0,170),		//rose				l_propotions_div_rectangle2,
	QColor(191,255,0),		//gren yellow		l_propotions_div_rectangle3,
	QColor(143,106,35),		//dark golden		l_propotions_div_ellipse,
	QColor(75,0,130),		//indigo			l_propotions_mult_rectangle,
	//QColor(237,185,185),	//pale rose			l_propotions_mirror_in_plane,
	//QColor(35,98,143),		//steel blue		l_propotions_mirror_2Dcurve ,
	//QColor(221,160,221),	//plum				l_propotions_mirror_tilted_plane,
	QColor(64,224,208),		//turquoise			l_hinging and rotating elements,
	QColor(143,35,35),		//maroon			l_ticks,
	QColor(249,255,94),	    //pale yellow		l_hatchng,	
	QColor(0,0,0),			//black				l_text,
	QColor(100,100,100),	    //gray		l_outline,
	QColor(249,255,94)    //pale yellow		l_shadow_construction,
};


string  line_type_name[26] =
{
	//Discriptive
	"smooth_contours",
	"ridges_visible",
	"valleys_visible",
	"ridges_occluded",	
	"valleys_occluded",
	"discriptive_cross_section",
	//Context
	"axis_and_grids",
	"scaffolds",
	"scaffolds_lines_to_vp",
	"scaffold_square",
	"scaffolds_circle_tangents",
	//Surfacing
	"surfacing_cross_section",
	"cross_section_scaffold",//"cross section for multiple rounding",
	"mirroring",
	"temporary_plane",
	"projection_lines",
	//Proprotions
	"divide_rectangle2",
	"divide_rectangle3",
	"divide_ellipse",
	"mult_rectangle",
	"hinging_and_rotating_elements",
	"ticks",
	//Other
	"hatching",		
	"text",
	"rough_outline",
	"shadow_construction"
};


//todo add ticks and other