#include "linesLabeling.h"
#include "globals.h"
#include <string>       // std::string
#include <iostream>     // std::cout
#include <sstream>      // std::stringstream
#include <iomanip>      // std::setw
#include "json.h"

LineLabeling::LineLabeling(UserDrawingLabeling *sketch, const string &folder)
:folder(folder)
{	
	//Name the page
	string title = "Lines labeling";
	setWindowTitle(tr(title.c_str()));

	//Intialise
	num_strokes = sketch->userStrokes.size();

	int sketch_dim = sketch->canvas_height;
	canvas_dim = min(sketch_dim, 1000); //QScreen *screen = QGuiApplication::primaryScreen(); QRect  screenGeometry = screen->geometry(); int height = screenGeometry.height(); int width = screenGeometry.width();
	to_label_canvas = new TabletCanvas(sketch_dim, sketch_dim);
	to_label_canvas->drawing = sketch;
	to_label_canvas->setFixedSize(canvas_dim, canvas_dim);	
	sketch_canvas = new TabletCanvas(sketch_dim, sketch_dim);
	sketch_canvas->drawing = sketch;
	sketch_canvas->setFixedSize(canvas_dim, canvas_dim);

	//Create layout
	QScrollArea *scrollArea = new QScrollArea(this);
	QWidget *viewport = new QWidget(this);	
	
	scrollArea->setWidgetResizable(true);	
	createLayout();

	
	mainLayout = new QVBoxLayout(viewport);
	mainLayout->addWidget(gridGroupBox);
	viewport->setLayout(mainLayout);
	scrollArea->setWidget(viewport);
	
	QHBoxLayout *dialog_layout = new QHBoxLayout(this);
	dialog_layout->setMargin(0);
	dialog_layout->setSpacing(0);
	dialog_layout->addWidget(scrollArea); // add scrollArea to the QDialog's layout
	setLayout(dialog_layout);


	intialLabeling();
	
	setAutoFillBackground(false);
	
	this->setMinimumWidth(canvas_dim + 800);
	this->setMinimumHeight(1150);	
}


void LineLabeling::intialLabeling()
{

	display(base_label);
	
	// Display initial sketch
	sketch_canvas->m_pixmap.fill(Qt::white);	
	scaleDrawing(sketch_canvas->m_pixmap);

	sketch_canvas->update();	
}


LineLabeling::~LineLabeling()
{
	delete layout;
	delete gridGroupBox;
	delete mainLayout;
}

void LineLabeling::changeTransparency(int alpha)
{
	transparency_slider->setValue(alpha);

	int first_stroke = time_scroll_slider_bottom->value();
	int last_stroke = time_scroll_slider_top->value();

	to_label_canvas->m_pixmap.fill(Qt::transparent);
	to_label_canvas->drawing->displaySketchInterval(to_label_canvas->m_pixmap, first_stroke, last_stroke, base_label, double(alpha) / 100.00, display_pressure);
	scaleDrawing(to_label_canvas->m_pixmap);
	to_label_canvas->update();
}


void LineLabeling::displayStrokeRange(int val) {

	int first_stroke = time_scroll_slider_bottom->value();
	int last_stroke = time_scroll_slider_top->value();

	stroke_first_box->setValue(first_stroke);
	stroke_last_box->setValue(last_stroke);

	double pressure_scale = double(transparency_slider->value()) / 100.0;

	to_label_canvas->m_pixmap.fill(Qt::transparent);

	to_label_canvas->drawing->displaySketchInterval(to_label_canvas->m_pixmap, first_stroke, last_stroke, base_label, pressure_scale, display_pressure);
	
	scaleDrawing(to_label_canvas->m_pixmap);
	
	to_label_canvas->update();
	selectSegmentsToLabel(base_label);
}


void LineLabeling::scaleDrawing(QPixmap &pixmap) {
	QPixmap scaled = pixmap.scaledToHeight(canvas_dim);
	pixmap.fill(Qt::transparent);
	QPainter painter(&pixmap);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.drawPixmap(0, 0, scaled);
	painter.end();
}



void LineLabeling::displayStrokeRangeBox() {
	int first_stroke = stroke_first_box->value();
	int last_stroke = stroke_last_box->value();

	time_scroll_slider_bottom->setValue(first_stroke);
	time_scroll_slider_top->setValue(last_stroke);

	double pressure_scale = double(transparency_slider->value()) / 100.0;

	to_label_canvas->m_pixmap.fill(Qt::transparent);
	to_label_canvas->drawing->displaySketchInterval(to_label_canvas->m_pixmap, first_stroke, last_stroke, base_label, pressure_scale, display_pressure);
	scaleDrawing(to_label_canvas->m_pixmap);
	to_label_canvas->update();

	selectSegmentsToLabel(base_label);
}


void LineLabeling::createLayout()
{
	gridGroupBox = new QGroupBox();
	layout = new QGridLayout;

	layout->addWidget(sketch_canvas, 0, 0, 1, 1, Qt::AlignTop);

	layout->addWidget(to_label_canvas, 0, 0, 1, 1, Qt::AlignTop);
	
	layout->addWidget(createMenuGroup(), 0, 1, 1, 1, Qt::AlignTop);
	
	gridGroupBox->setLayout(layout);

}

QGroupBox* LineLabeling::createExclusiveLabelDisplayGroup()
{
	QGroupBox *groupBox = new QGroupBox(tr("Select what to relabel"));
	groupBox->setFlat(true);
	base_label = l_text;

	for (int i = 0; i < to_label_canvas->drawing->strokeSegments.size(); i++)
		if (to_label_canvas->drawing->strokeSegments[i]->line_type_ == -1)
			to_label_canvas->drawing->strokeSegments[i]->line_type_ = base_label;

	QVBoxLayout *vbox = new QVBoxLayout;
	QSignalMapper* signalMapper = new QSignalMapper(this);


	QLabel* discriptive_text = new QLabel("Discriptive");
	vbox->addWidget(discriptive_text);

	for (int i = 0; i <= l_cross_sections; i++)
	{
		createRuddioBttn(i, line_type_name[i], vbox, signalMapper);
	}

	//Context
	QLabel* construction_text = new QLabel("Context:");
	vbox->addWidget(construction_text);
	for (int i = l_context_axis_and_grids; i <= l_context_scaffold_circle; i++)
	{
		createRuddioBttn(i, line_type_name[i], vbox, signalMapper);
	}

	//Surfacing
	QLabel* surfacing_text = new QLabel("Surfacing:");
	vbox->addWidget(surfacing_text);
	for (int i = l_surfacing_cross_section; i <= l_surfacing_projection_lines; i++)
	{
		createRuddioBttn(i, line_type_name[i], vbox, signalMapper);
	}


	//Propotions
	QLabel* propotions_text = new QLabel("Propotions:");
	vbox->addWidget(propotions_text);
	for (int i = l_propotions_div_rectangle2; i <= l_ticks; i++)
	{
		createRuddioBttn(i, line_type_name[i], vbox, signalMapper);
	}

	for (int i = l_hatching; i <= l_shadow_construction; i++)
	{
		createRuddioBttn(i, line_type_name[i], vbox, signalMapper);
	}


	//Deafault
	lines_types[base_label]->setChecked(true);
	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(display(int)));

	groupBox->setLayout(vbox);

	return groupBox;
}


void LineLabeling::set_label_color(QRadioButton *bttn, int line_type) {
	QPalette* palette = new QPalette();
	palette->setColor(QPalette::Foreground, labels_colors_[line_type]);
	bttn->setPalette(*palette);
}

void  LineLabeling::createRuddioBttnLabel(int line_type, const string s, QVBoxLayout *vbox, QSignalMapper* signalMapper) {
	const char* name_bttn = s.c_str();
	lines_types_label[line_type] = new QRadioButton(tr(name_bttn));
	vbox->addWidget(lines_types_label[line_type]);
	connect(lines_types_label[line_type], SIGNAL(clicked()), signalMapper, SLOT(map()));
	signalMapper->setMapping(lines_types_label[line_type], line_type);
	set_label_color(lines_types_label[line_type], line_type);
}

void  LineLabeling::createRuddioBttn(int line_type, const string s, QVBoxLayout *vbox, QSignalMapper* signalMapper) {
	const char* name_bttn = s.c_str();
	lines_types[line_type] = new QRadioButton(tr(name_bttn));
	vbox->addWidget(lines_types[line_type]);
	connect(lines_types[line_type], SIGNAL(clicked()), signalMapper, SLOT(map()));
	signalMapper->setMapping(lines_types[line_type], line_type);
}

QGroupBox* LineLabeling::createExclusiveLabelColorGroup()
{
	QGroupBox *groupBox = new QGroupBox(tr("Select the label value"));
	groupBox->setFlat(true);
	groupBox->setStyleSheet("background-color:white;");
	

	QVBoxLayout *vbox = new QVBoxLayout;
	QSignalMapper* signalMapper = new QSignalMapper(this);

	QLabel* discriptive_text = new QLabel("Discriptive");
	vbox->addWidget(discriptive_text);

	for (int i = 0; i <= l_cross_sections; i++)
	{
		createRuddioBttnLabel(i, line_type_name[i], vbox, signalMapper);
	}

	//Context
	QLabel* construction_text = new QLabel("Context:");
	vbox->addWidget(construction_text);
	for (int i = l_context_axis_and_grids; i <= l_context_scaffold_circle; i++)
	{
		createRuddioBttnLabel(i, line_type_name[i], vbox, signalMapper);
	}

	//Surfacing
	QLabel* surfacing_text = new QLabel("Surfacing:");
	vbox->addWidget(surfacing_text);
	for (int i = l_surfacing_cross_section; i <= l_surfacing_projection_lines; i++)
	{
		createRuddioBttnLabel(i, line_type_name[i], vbox, signalMapper);
	}


	//Propotions
	QLabel* propotions_text = new QLabel("Propotions:");
	vbox->addWidget(propotions_text);
	for (int i = l_propotions_div_rectangle2; i <= l_ticks; i++)
	{
		createRuddioBttnLabel(i, line_type_name[i], vbox, signalMapper);
	}

	for (int i = l_hatching; i <= l_shadow_construction; i++)
	{
		createRuddioBttnLabel(i, line_type_name[i], vbox, signalMapper);
	}


	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(setLabel(int)));	


	assignBttn = new QPushButton(tr("Assign"));
	vbox->addWidget(assignBttn);
	connect(assignBttn, SIGNAL(clicked()), this, SLOT(assignLabelToSelection()));

	groupBox->setLayout(vbox);

	return groupBox;
}

QGroupBox* LineLabeling::createDisplayPannelGroup()
{
	QGroupBox *groupBox = new QGroupBox(tr("Display pannel"));
	//groupBox->setFlat(true);
	
	QVBoxLayout *vbox = new QVBoxLayout;

	vbox->addWidget(createExclusiveLabelDisplayGroup());
	vbox->addWidget(createExclusivePressureGroup());

	//Slider for an intial drawing:
	QLabel* construction_text = new QLabel("Visiability of the initial drawing:");
	vbox->addWidget(construction_text);
	transparency_slider = new QSlider(Qt::Horizontal);
	transparency_slider->setMinimum(0);
	transparency_slider->setMaximum(100);
	transparency_slider->setSingleStep(1);
	transparency_slider->setValue(transparency * 100);
	connect(transparency_slider, SIGNAL(valueChanged(int)), this, SLOT(changeTransparency(int)));
	vbox->addWidget(transparency_slider);

	groupBox->setLayout(vbox);

	return groupBox;
}

QGroupBox* LineLabeling::createLabelPannelGroup()
{
	QGroupBox *groupBox = new QGroupBox(tr("Label pannel"));

	QVBoxLayout *vbox = new QVBoxLayout;
	
	vbox->addWidget(createExclusiveLabelColorGroup());

	groupBox->setLayout(vbox);

	return groupBox;
}


QGroupBox* LineLabeling::createMenuGroup() 
{
	QGroupBox *menu_groupBox = new QGroupBox();
	menu_groupBox->setFlat(true);

	QVBoxLayout *vbox = new QVBoxLayout;

	//Two labels pannels
	QGroupBox *pannels_groupBox = new QGroupBox();
	pannels_groupBox->setFlat(true);
	QHBoxLayout *hbox = new QHBoxLayout;
	hbox->addWidget(createDisplayPannelGroup());
	hbox->addWidget(createLabelPannelGroup());
	pannels_groupBox->setLayout(hbox);

	vbox->addWidget(pannels_groupBox);

	//create time slider
	QLabel* time_scroll_slider_top_label= new QLabel("Last stroke");
	vbox->addWidget(time_scroll_slider_top_label);
	time_scroll_slider_top = new QSlider(Qt::Horizontal);
	time_scroll_slider_top->setMinimum(0);
	time_scroll_slider_top->setMaximum(num_strokes-1);
	time_scroll_slider_top->setSingleStep(1);
	time_scroll_slider_top->setValue(num_strokes);
	connect(time_scroll_slider_top, SIGNAL(valueChanged(int)), this, SLOT(displayStrokeRange(int)));
	vbox->addWidget(time_scroll_slider_top);

	QLabel* time_scroll_slider_bottom_label = new QLabel("First stroke");
	vbox->addWidget(time_scroll_slider_bottom_label);
	time_scroll_slider_bottom = new QSlider(Qt::Horizontal);
	time_scroll_slider_bottom->setMinimum(0);
	time_scroll_slider_bottom->setMaximum(num_strokes-1);
	time_scroll_slider_bottom->setSingleStep(1);
	time_scroll_slider_bottom->setValue(0);
	connect(time_scroll_slider_bottom, SIGNAL(valueChanged(int)), this, SLOT(displayStrokeRange(int)));
	vbox->addWidget(time_scroll_slider_bottom);

	
	stroke_first_box = new QSpinBox();
	stroke_last_box = new QSpinBox();
	stroke_first_box->setValue(0);
	stroke_last_box->setValue(num_strokes - 1);
	stroke_first_box->setSingleStep(1);
	stroke_last_box->setSingleStep(1);

	stroke_first_box->setMinimum(0);
	stroke_first_box->setMaximum(num_strokes - 1);

	stroke_last_box->setMinimum(0);
	stroke_last_box->setMaximum(num_strokes - 1);

	connect(stroke_first_box, SIGNAL(valueChanged(int)), this, SLOT(displayStrokeRangeBox()));
	connect(stroke_last_box, SIGNAL(valueChanged(int)), this, SLOT(displayStrokeRangeBox()));

	vbox->addWidget(new QLabel("First stroke value"));
	vbox->addWidget(stroke_first_box);

	vbox->addWidget(new QLabel("Last stroke value"));
	vbox->addWidget(stroke_last_box);

	//Two save buttons:

	//Two labels pannels
	QGroupBox *save_buttons_groupBox = new QGroupBox();
	save_buttons_groupBox->setFlat(true);

	QHBoxLayout *hbox_bttns = new QHBoxLayout;
	saveImgsBttn = new QPushButton(tr("Save"));
	hbox_bttns->addWidget(saveImgsBttn);
	connect(saveImgsBttn, SIGNAL(clicked()), this, SLOT(saveLabelingImages()));

	saveImgsGroupedBttn = new QPushButton(tr("Save Grouped"));
	hbox_bttns->addWidget(saveImgsGroupedBttn);
	connect(saveImgsGroupedBttn, SIGNAL(clicked()), this, SLOT(saveLabelingImagesGrouped()));

	saveImgsTimeBttn = new QPushButton(tr("Save Images Selected Interval"));
	hbox_bttns->addWidget(saveImgsTimeBttn);
	connect(saveImgsTimeBttn, SIGNAL(clicked()), this, SLOT(saveImagesSelectedInterval()));
	
	save_buttons_groupBox->setLayout(hbox_bttns);

	vbox->addWidget(save_buttons_groupBox);


	menu_groupBox->setLayout(vbox);
	return menu_groupBox;
}

QGroupBox* LineLabeling::createExclusivePressureGroup()
{
	QGroupBox *groupBox = new QGroupBox(tr("Show with pressure value"));
	groupBox->setFlat(true);

	pressure_yes = new QRadioButton(tr("Yes"));
	pressure_no  = new QRadioButton(tr("No"));
	

	pressure_yes->setChecked(true);
	display_pressure = true;

	QHBoxLayout *hbox = new QHBoxLayout;
	hbox->addWidget(pressure_yes);
	hbox->addWidget(pressure_no);
	
	QSignalMapper* signalMapper = new QSignalMapper(this);

	connect(pressure_yes, SIGNAL(clicked()), signalMapper, SLOT(map()));
	connect(pressure_no, SIGNAL(clicked()), signalMapper, SLOT(map()));
	
	signalMapper->setMapping(pressure_yes, 1);
	signalMapper->setMapping(pressure_no, 0);

	connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(setDisplayPressure(int)));

	groupBox->setLayout(hbox);

	return groupBox;
}



void LineLabeling::assignLabelToSelection() 
{
	UserDrawingLabeling* sketch = to_label_canvas->drawing;
	
	for (int i = 0; i < num_seg_to_label.size(); i++)
		sketch->strokeSegments[num_seg_to_label[i]]->line_type_ = assign_label;

	saveLabeling();
	display(base_label);
}


void LineLabeling::selectSegmentsToLabel(int line_type)
{
	UserDrawingLabeling* sketch = to_label_canvas->drawing;
	num_seg_to_label.clear();
	

	int first_stroke = time_scroll_slider_bottom->value();
	int last_stroke = time_scroll_slider_top->value();

	int first_segment;
	int last_segment;

	for (int i = first_stroke; i <= last_stroke; i++)
	{
		if (sketch->userStrokes[i]->is_removed_)
			continue;
	

		first_segment = sketch->userStrokes[i]->first_segment_ind;
		last_segment = sketch->userStrokes[i]->last_segment_ind;

		for (int j = first_segment; j <= last_segment; j++)
		{
			if (sketch->strokeSegments[j]->line_type_ == line_type)
				num_seg_to_label.push_back(j);
		}
	}
}

void LineLabeling::display(int line_type)
{	
	base_label = line_type;	

	displayStrokeRange();	
};

void LineLabeling::saveImagesSelectedInterval()
{
	UserDrawingLabeling *sketch = to_label_canvas->drawing;
	QPixmap image = QPixmap(sketch->canvas_width, sketch->canvas_height);
	image.fill(Qt::white);

	int first_stroke = time_scroll_slider_bottom->value();
	int last_stroke = time_scroll_slider_top->value();

	double pressure_scale = 1.0;
	bool display_pressure = true;
	bool use_label_color = false;

	sketch->displaySketchInterval(image, first_stroke, last_stroke, base_label, pressure_scale, display_pressure, use_label_color);

	string folder_intervals = folder + "//intervals";
	cout << folder_intervals << endl;
	QString folder_intervals_q(folder_intervals.c_str());

	QDir dir(folder_intervals_q);
	if (!dir.exists()) {
		dir.mkpath(folder_intervals_q);
	}

	int first_stroke_present = sketch->userStrokes[first_stroke]->ind_non_removed;
	int last_stroke_present = sketch->userStrokes[last_stroke]->ind_non_removed;

	string filename = folder_intervals + "//first_stroke_" + to_string(first_stroke_present) +
									    "_last_stroke_" + to_string(last_stroke_present) +".png";
	image.save(QString(filename.c_str()));




	QPixmap image2 = QPixmap(sketch->canvas_width, sketch->canvas_height);
	image2.fill(Qt::white);
	sketch->displaySketchInterval(image2, first_stroke, last_stroke, base_label, pressure_scale, display_pressure, true);
	filename = folder_intervals + "//first_stroke_" + to_string(first_stroke_present) +
		"_last_stroke_" + to_string(last_stroke_present) + "_labels.png";
	image2.save(QString(filename.c_str()));



	QPixmap image3 = QPixmap(sketch->canvas_width, sketch->canvas_height);
	image3.fill(Qt::white);
	sketch->displaySketchInterval(image3, first_stroke, last_stroke, base_label, pressure_scale, false, true);
	filename = folder_intervals + "//first_stroke_" + to_string(first_stroke_present) +
		"_last_stroke_" + to_string(last_stroke_present) + "_labels_equal_pressure.png";
	image3.save(QString(filename.c_str()));


};

void  LineLabeling::saveLabelingImages()
{
	saveLabeling();
	UserDrawingLabeling *sketch = to_label_canvas->drawing;
	QPixmap image = QPixmap(sketch->canvas_width, sketch->canvas_height);
	image.fill(Qt::white);

	for (int i = 0; i < num_types_of_lines; i++)
		sketch->displayLineType(image, i, true);

	string filename = folder + "//labeling.png";
	image.save(QString(filename.c_str()));


	//save layers
	string folder_layers = folder + "//layers";

	if (!QDir(QString(folder_layers.c_str())).exists())
		QDir().mkdir(QString(folder_layers.c_str()));

	folder_layers = folder + "//layers//transparent";
	if (!QDir(QString(folder_layers.c_str())).exists())
		QDir().mkdir(QString(folder_layers.c_str()));

	for (int i = 0; i < num_types_of_lines; i++)
	{
		QPixmap image = QPixmap(sketch->canvas_width, sketch->canvas_height);
		image.fill(Qt::transparent);
		sketch->displayDrawing(image, 0.8);



		if (sketch->displayLineType(image, i, false))
		{
			std::stringstream ss;
			ss << std::setw(2) << std::setfill('0') << i;
			std::string num = ss.str();

			filename = folder + "//layers//" + num + '_' + line_type_name[i] + ".png";
			image.save(QString(filename.c_str()));
		}

		image.fill(Qt::transparent);

		if (sketch->displayLineType(image, i, false))
		{
			std::stringstream ss;
			ss << std::setw(2) << std::setfill('0') << i;
			std::string num = ss.str();

			filename = folder + "//layers//transparent//" + num + '_' + line_type_name[i] + "_transparent.png";
			image.save(QString(filename.c_str()));
		}

	}


	//Without pressure
	image.fill(Qt::white);
	sketch->displayLineTypes(image, true);



	filename = folder + "//labeling_width.png";
	image.save(QString(filename.c_str()));

	image.fill(Qt::white);
	sketch->displayLineTypes(image, false);



	filename = folder + "//labeling_constant_width.png";
	image.save(QString(filename.c_str()));

	image.fill(Qt::transparent);
	sketch->displayLineTypes(image, false);



	filename = folder + "//labeling_constant_width_transparent.png";
	image.save(QString(filename.c_str()));

	image.fill(Qt::transparent);
	sketch->displayLineTypes(image, true);



	filename = folder + "//labeling_width_transaprent.png";
	image.save(QString(filename.c_str()));
}


	
//Save only images grouped as in figures with line types appearance depending on time:
void  LineLabeling::saveLabelingImagesGrouped()
{	
	UserDrawingLabeling *sketch = to_label_canvas->drawing;	

	//Save grouped layers:
	//Create folders if does not exist
	string folder_layers = folder + "//layers";
	if (!QDir(QString(folder_layers.c_str())).exists())
		QDir().mkdir(QString(folder_layers.c_str()));

	folder_layers = folder + "//layers//grouped//";
	if (!QDir(QString(folder_layers.c_str())).exists())
		QDir().mkdir(QString(folder_layers.c_str()));

	
	string filename;
	bool use_pressure = false;

	// Descriptive
		QPixmap image = QPixmap(sketch->canvas_width, sketch->canvas_height);
		image.fill(Qt::white);
		//Group smooth and creases
		for (int i = l_occluding_contour; i < l_valleys_occluded; i++)
		{
			sketch->displayLineType(image, i, labels_colors_[l_occluding_contour], use_pressure);
		}
		//Add cross-sections
		sketch->displayLineType(image, l_cross_sections, use_pressure);
		//Save image
		filename = folder_layers + "descriptive.png";
		image.save(QString(filename.c_str()));


	// Context
		image.fill(Qt::white);
		//Add axis and grids
		sketch->displayLineType(image, l_context_axis_and_grids, use_pressure);

		//Group scaffolds
		for (int i = l_context_scaffolds; i < l_context_scaffold_circle; i++)
		{
			sketch->displayLineType(image, i, labels_colors_[l_context_scaffolds], use_pressure);
		}
		//Save image
		filename = folder_layers + "context.png";
		image.save(QString(filename.c_str()));


	// Proportions + Surfacing:
		image.fill(Qt::white);
		//Cross section
		for (int i = l_surfacing_cross_section; i < l_surfacing_cross_section_multiple_rounding; i++)
		{
			sketch->displayLineType(image, i, labels_colors_[l_surfacing_cross_section], use_pressure);
		}
		
		//Other surfacing lines, each with its own label
		for (int i = l_surfacing_mirroring; i < l_surfacing_projection_lines; i++)
		{
			sketch->displayLineType(image, i, use_pressure);
		}

		//Group Proportions
		for (int i = l_propotions_div_rectangle2; i < l_hinging_and_rotating_elements; i++)
		{
			sketch->displayLineType(image, i, labels_colors_[l_propotions_div_rectangle2], use_pressure);
		}

		//Add Ticks
		sketch->displayLineType(image, l_ticks, use_pressure);

		//Save image
		filename = folder_layers + "proportions_surfacing.png";
		image.save(QString(filename.c_str()));
}

void LineLabeling::saveInitialDrawing()
{
	UserDrawingLabeling *sketch = to_label_canvas->drawing;

	QColor color(0,0,0);

	QPixmap image = QPixmap(sketch->canvas_width, sketch->canvas_height);
	image.fill(Qt::white);



	for (int j = 0; j < sketch->strokeSegments.size(); j++)
	{

		sketch->drawStrokeSegment(j, image, color);
	}

	string folder_labels_train = folder + "//labels_grouped";

	QDir dir(folder_labels_train.c_str());
	if (!dir.exists())
	{
		QDir().mkdir(folder_labels_train.c_str());
	}

	string filename = folder_labels_train + "//sketch.png";
	image.save(QString(filename.c_str()));
}



void LineLabeling::setLabel(int line_type) 
{
	assign_label = line_type;
}


void LineLabeling::saveLabeling()
{

	QString fileSaveLabelling(filename_global.c_str());
	fileSaveLabelling.replace(QString(".json"), QString("_strokes_labeling.json"));

	QString path = QDir::currentPath() + fileSaveLabelling;
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save labeling as a json file"),
													path);

	
	QJsonObject root;
	QJsonObject labelsMeaning;
	QJsonArray strokesLabelsArray;

	UserDrawingLabeling* sketch = to_label_canvas->drawing;

	std::vector< StrokeSegment* > segments = sketch->strokeSegments;

	int stroke_num;

	//Assign a line type to a stroke:
	for (int i = 0; i < static_cast<int>(segments.size()); ++i)
	{
		stroke_num = segments[i]->userStrokeIndex;
		if (!sketch->userStrokes[stroke_num]->is_removed_)
		{		
			sketch->userStrokes[stroke_num]->line_type_ = segments[i]->line_type_; //all segments have the same label withing a stroke
		}
	}

	for (int stroke_num = 0; stroke_num < static_cast<int>(sketch->userStrokes.size()); ++stroke_num)
	{
		if (!sketch->userStrokes[stroke_num]->is_removed_)
		{
			strokesLabelsArray.push_back(sketch->userStrokes[stroke_num]->line_type_);
		}
	}

	root.insert("strokes_line_types", strokesLabelsArray);

	QJsonDocument doc(root);
	QFile jsonFile;

	jsonFile.setFileName(fileName);
	jsonFile.open(QIODevice::WriteOnly | QIODevice::Text);
	jsonFile.write(doc.toJson());
	jsonFile.close();
}