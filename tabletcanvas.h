/*
The TabletCanvas class inherits QWidget and receives tablet events.
It uses the events to paint on a offscreen pixmap, which it draws onto itself.
The TabletCanvas class provides a surface on which the user can draw with a tablet.

When you use a tablet with Qt applications, QTabletEvents are generated. You need to 
reimplement the tabletEvent() event handler if you want to handle tablet events. Events 
are generated when the tool (stylus) used for drawing enters and leaves the proximity of the tablet 
(i.e., when it is close but not pressed down on it), when the tool is pressed down and released from 
it, when the tool is moved across the tablet, and when one of the buttons on the tool is pressed or released.
*/

#ifndef TABLETCANVAS_H
#define TABLETCANVAS_H

#include <QWidget>
#include <QPixmap>
#include <QPoint>
#include <QTabletEvent>
#include <QColor>
#include <QBrush>
#include <QPen>


#include "drawing.h"

class QPaintEvent;
class QString;

class TabletCanvas : public QWidget
{
	Q_OBJECT

public:
	enum Valuator {
		PressureValuator, TangentialPressureValuator,
		TiltValuator, VTiltValuator, HTiltValuator, NoValuator,
		NoPressureValuator
	};

	Q_ENUM(Valuator)

		TabletCanvas(int height = 500, int width = 500);
		~TabletCanvas();

	void displayPixmap(QPixmap &pixmap);
	bool saveImage(const QString &file);
	

	void setAlphaChannelValuator(Valuator type)
	{
		m_alphaChannelValuator = type;
	}
	void setColorSaturationValuator(Valuator type)
	{
		m_colorSaturationValuator = type;
	}
	void setLineWidthType(Valuator type)
	{
		m_lineWidthValuator = type;
	}
	void setColor(const QColor &c)
	{
		if (c.isValid()) m_color = c;
	}
	QColor color() const
	{
		return m_color;
	}
	void setTabletDevice(QTabletEvent *event)
	{
		updateCursor(event);
	}
	int maximum(int a, int b)
	{
		return a > b ? a : b;
	}

	int height(){
		return m_height;
	}

	int width(){
		return m_width;
	}

	void clear();
	/*void play();*/
	//void serialize(QString filename); //Write to JSON file

	UserDrawingLabeling* drawing;
	QPixmap m_pixmap;
	void updatePixmap();
	void initPixmap();

protected:
	void tabletEvent(QTabletEvent *event) override;
	void paintEvent(QPaintEvent *event) override;
	void resizeEvent(QResizeEvent *event) override;

private:
	
	int stroke_index;

	int startTime;
	int currTime;



	void paintPixmap(QPainter &painter, QTabletEvent *event);
	//Qt::BrushStyle brushPattern(qreal value);
	void updateBrush(const QTabletEvent *event);
	void updateCursor(const QTabletEvent *event);

	Valuator m_alphaChannelValuator;
	Valuator m_colorSaturationValuator;
	Valuator m_lineWidthValuator;
	QColor m_color;

	QBrush m_brush;
	double pen_width;
	QPen m_pen;
	bool m_deviceDown;
	int m_height;	// Height of the wigjet
	int m_width;	// Width of the wigjet

	struct PointT {
		QPointF pos;
		qreal rotation;
	} lastPoint;
};

#endif